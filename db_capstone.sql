-- DROP DATABASE db_capstone;
CREATE DATABASE db_capstone;
USE db_capstone;

-- CREATE
CREATE TABLE nguoi_dung(
	nguoi_dung_id 	INT PRIMARY KEY AUTO_INCREMENT,
	email 			VARCHAR(255),
	mat_khau		VARCHAR(255),
	ho_ten			VARCHAR(255),
	tuoi			INT,
	anh_dai_dien	TEXT
);
CREATE TABLE hinh_anh(
	hinh_id 		INT PRIMARY KEY AUTO_INCREMENT,
	ten_hinh 		VARCHAR(255),
	duong_dan 		TEXT,
	mo_ta 			VARCHAR(255),
	nguoi_dung_id 	INT,
	CONSTRAINT fk_nguoidungid_hinhanh_nguoidung FOREIGN KEY (nguoi_dung_id) REFERENCES nguoi_dung(nguoi_dung_id) 
);
CREATE TABLE binh_luan(
	binh_luan_id 	INT PRIMARY KEY AUTO_INCREMENT,
	nguoi_dung_id	INT,
	hinh_id			INT,
	ngay_binh_luan	DATE,
	noi_dung		VARCHAR(255),
	CONSTRAINT fk_nguoidungid_binhluan_nguoidung FOREIGN KEY (nguoi_dung_id) REFERENCES nguoi_dung(nguoi_dung_id),
	CONSTRAINT fk_hinhanhid_binhluan_hinhanh FOREIGN KEY (hinh_id) REFERENCES hinh_anh(hinh_id)
);
CREATE TABLE luu_anh(
	nguoi_dung_id	INT,
	hinh_id			INT,
	ngay_luu		DATE,
	PRIMARY KEY (nguoi_dung_id, hinh_id),
	CONSTRAINT fk_nguoidungid_luuanh_nguoidung FOREIGN KEY (nguoi_dung_id) REFERENCES nguoi_dung(nguoi_dung_id),
	CONSTRAINT fk_hinhanhid_luuanh_hinhanh FOREIGN KEY (hinh_id) REFERENCES hinh_anh(hinh_id)
);

-- INSERT 
INSERT INTO nguoi_dung(email,mat_khau,ho_ten,tuoi,anh_dai_dien)
VALUES	('nguyensythanh11@gmail.com','123456','Nguyễn Sỹ Thành',20,null),
		('nguyensythang@gmail.com','123456','Nguyễn Sỹ Thắng',20,null);
INSERT INTO hinh_anh(ten_hinh,duong_dan,mo_ta,nguoi_dung_id)
VALUES	('Mèo','Đường dẫn 1','Dễ thương',1),
		('Chó','Đường dẫn 2','Ngầu',1);
INSERT INTO binh_luan(nguoi_dung_id,hinh_id,ngay_binh_luan,noi_dung)
VALUES	(1,1,'2023-11-01','Con mèo này của tôi'),
		(2,1,'2023-11-01','Con mèo này thật dễ thương'),
		(2,2,'2023-11-01','Con chó này thật dễ thương');
INSERT INTO luu_anh(nguoi_dung_id,hinh_id,ngay_luu)
VALUES	(2,1,'2023-11-01'),
		(2,2,'2023-11-01');